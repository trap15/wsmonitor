# Simple makefile for doing non-build things, and invoking tup I guess

define tup-build =
mkdir -p build-$@
cp configs/$@.config build-$@/tup.config
tup build-$@
endef

.PHONY: all clean \
	wswan \
	wwsoft

DEFAULT_BUILDS := wswan

all: $(DEFAULT_BUILDS)

wswan:
	$(tup-build)
wwsoft:
	$(tup-build)

clean:
	rm -rf build-*
