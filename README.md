# wsmonitor

wsmonitor is an 80186 monitor particularly designed for use on the
Bandai WonderSwan. In reality, it'll work on any 80186-compatible device,
and probably any 8086 device with a few tweaks.

The source code is written for MASM. You'll need [tup](http://gittup.org/tup/) to build the source,
and my [trptool](http://bitbucket.org/trap15/trptool/) to build the WonderSwan version.

The WonderSwan version builds as both a native WonderSwan ROM and a WonderWitch
`soft` program.


# Prompt specifics.

Lower-case input is not supported. All lower-case inputs will be read as upper-case.

Preceding and suffixed spaces are ignored. Commands are a single character.
These combined means instead of entering `R AX`, you could enter `RAX` and
get the same result with less typing.

Lines are limited to a maximum of 48 characters including ending NUL.
Exceeding this limit will force the line to be completed, causing the line
to execute.


# Requirements

Total required RAM is 04Ch bytes plus the line size (default 030h bytes).

Required ROM/program space is currently approximately 1.5KB. This could probably
be improved somewhat, but I think it's pretty acceptable.

IRQs are not used and should probably be disabled. No exception handlers
are installed.


# Configuration

`config.inc` contains configuration options that you may wish to change.

### V_WORK_SEG

Defines the segment for work RAM area. THIS SHOULD BE THE SAME AS THE
ORIGINAL PROGRAM'S SS SEGMENT.

### V_WORK_AREA

Defines the address inside `V_WORK_SEG` to use for work RAM.

### HAS_RX_IRQ

Defines whether the platform supports a serial RX IRQ. This can be helpfully
used to reduce power consumption by use of `HLT`.

### INPUT_LINE_SIZE

Maximum characters allowed in an input line.


# Porting

`raw_serial.asm` should be the only platform-specific code. Some 80186
instructions are used, though it could work fine on 8086 with some tweaks.

`main.asm` and `crt0ws.asm` are both specific to running the monitor standalone
on WonderSwan, it's likely you can delete these files.


# Command listing

### D address[,count] -- Dump memory

Dumps memory. If count is unspecified, it will dump 040h bytes.
Addresses are in flat addressing scheme. Sizes are rounded up to multiples of 010h.


### E address[,byte[,byte[...]]] -- Enter memory

Writes bytes to memory. Each byte is written sequentially.


### G -- Go

Resumes execution from where the monitor broke in.


### I port[,size] -- Port In

Read from an I/O port. Size is B for byte, W for word. Defaults to word.


### O port,value[,size] -- Port Out

Write to an I/O port. Size is B for byte, W for word. Defaults to word.


### Q -- Reset

Resets the monitor.


### R [register name[,[new value]]] -- Show/set register

With no register specified, all registers are dumped.
With a register specified, only that register is dumped.
If a comma follows the register name, the register will be set.
If no new value is specified after the comma, the value will be prompted.


# Cool Links

* <http://daifukkat.su/> - My website
* <https://bitbucket.org/trap15/wsmonitor> - This repository

# Greetz

* \#raidenii - Forever impossible
* SCP-300's 8086 monitor, from which the general design of this monitor was taken

# Licensing

All contents within this repository (unless otherwise specified) are licensed
under the following terms:

The MIT License (MIT)

Copyright (c) 2016 Alex 'trap15' Marshall

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
