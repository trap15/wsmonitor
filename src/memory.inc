V_STACK_SPACE		equ	020h
V_WORK_SIZE		equ	02Ch+INPUT_LINE_SIZE+V_STACK_SPACE
V_STACK_TOP		equ	V_WORK_AREA+V_WORK_SIZE

MonitorWork	STRUCT
	regsave_ax	WORD	? ; 0
	regsave_cx	WORD	? ; 2
	regsave_dx	WORD	? ; 4
	regsave_bx	WORD	? ; 6
	regsave_sp	WORD	? ; 8
	regsave_bp	WORD	? ; 10
	regsave_si	WORD	? ; 12
	regsave_di	WORD	? ; 14
	regsave_cs	WORD	? ; 16
	regsave_ds	WORD	? ; 18
	regsave_es	WORD	? ; 20
	regsave_ss	WORD	? ; 22
	regsave_pc	WORD	? ; 24
	regsave_flag	WORD	? ; 26
	input_line	BYTE	INPUT_LINE_SIZE dup (?)
	cmd_work	BYTE	16 dup (?)
MonitorWork	ENDS
