; Implements:
;   void serial_send_str(const char far *ptr);
;   void serial_send_binary(WORD value, WORD length);
;   void serial_send_hex(WORD value, WORD length);
;   void serial_send_lptr(void far *ptr);
;   void serial_send_newline(void);
;   void serial_recv_buf(WORD len, void far *ptr);
;   BYTE serial_recv_line(void);

	.186

	INCLUDE	<top.inc>

	EXTRN	_serial_recv_byte:proc
	EXTRN	_serial_send_byte:proc

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
; SS:SI is the work area!
	ASSUME	SI:PTR MonitorWork

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_send_str(const char far *ptr);
	PUBLIC	_serial_send_str
_serial_send_str::
	PUSH	BP
	MOV	BP, SP
	PUSH	ES
	PUSH	BX

	LES	BX, [BP+4]
@@:
	MOV	AL, ES:[BX]
	TEST	AL, AL
	JZ	@F
	PUSH	AX
	CALL	_serial_send_byte
	INC	BX
	JMP	@B

@@:
	POP	BX
	POP	ES
	POP	BP
	RET	4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_send_binary(WORD value, WORD length);
	PUBLIC	_serial_send_binary
_serial_send_binary::
	PUSH	BP
	MOV	BP, SP
	PUSH	CX
	PUSH	DX
	PUSH	BX

	MOV	BX, hex_xlat
	MOV	AX, 16
	SUB	AX, [BP+6]
	MOV	CX, AX
	MOV	AX, [BP+4]
	SHL	AX, CL
	MOV	CX, [BP+6]
@@:
	ROL	AX, 1
	MOV	DX, AX
	AND	AX, 1
	XLAT
	PUSH	AX
	CALL	_serial_send_byte
	MOV	AX, DX
	LOOP	@B

	POP	BX
	POP	DX
	POP	CX
	POP	BP
	RET	4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_send_hex(WORD value, WORD length);
	PUBLIC	_serial_send_hex
_serial_send_hex::
	PUSH	BP
	MOV	BP, SP
	PUSH	CX
	PUSH	DX
	PUSH	BX

	MOV	BX, hex_xlat
	MOV	AX, 4
	SUB	AX, [BP+6]
	SHL	AX, 2
	MOV	CX, AX
	MOV	AX, [BP+4]
	SHL	AX, CL
	MOV	CX, [BP+6]
@@:
	ROL	AX, 4
	MOV	DX, AX
	AND	AX, 0Fh
	XLAT
	PUSH	AX
	CALL	_serial_send_byte
	MOV	AX, DX
	LOOP	@B

	POP	BX
	POP	DX
	POP	CX
	POP	BP
	RET	4

hex_xlat::
	DB	"0123456789ABCDEF"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_send_lptr(void far *ptr);
	PUBLIC	_serial_send_lptr
_serial_send_lptr::
	PUSH	BP
	MOV	BP, SP

	MOV	AX, [BP+6]
	PUSH	4
	PUSH	AX
	CALL	_serial_send_hex

	PUSH	':'
	CALL	_serial_send_byte

	MOV	AX, [BP+4]
	PUSH	4
	PUSH	AX
	CALL	_serial_send_hex

	POP	BP
	RET	4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_send_newline(void);
	PUBLIC	_serial_send_newline
_serial_send_newline::
	PUSH	_CR
	CALL	_serial_send_byte
	PUSH	_LF
	CALL	_serial_send_byte
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; void serial_recv_buf(WORD len, void far *ptr);
	PUBLIC	_serial_recv_buf
_serial_recv_buf::
	PUSH	BP
	MOV	BP, SP
	PUSH	ES
	PUSH	CX
	PUSH	BX

	MOV	CX, [BP+4]
	LES	BX, [BP+6]
@@:
	CALL	_serial_recv_byte
	MOV	ES:[BX], AL
	INC	BX
	LOOP	@B

	POP	BX
	POP	CX
	POP	ES
	POP	BP
	RET	6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; BYTE serial_recv_line(void);
; Implicitly reads to work.input_line
; Cancels set input_line to ""
; Returns first character
	PUBLIC	_serial_recv_line
_serial_recv_line::
	PUSH	CX
	PUSH	BX

	LEA	DI, SS:[SI].input_line
	MOV	CX, INPUT_LINE_SIZE - 1
@@:
	CALL	_serial_recv_byte
	MOV	BX, low_xlat
	CMP	AL, 020h
	JL	__linerecv_xlat
	CMP	AL, 060h
	JL	__linerecv_normal
__linerecv_high::
	MOV	BX, high_xlat-60h
__linerecv_xlat::
	XLAT
	CMP	AL, _CC
	JE	__linerecv_cancel
	CMP	AL, _BS
	JNE	__linerecv_normal
	CMP	CX, INPUT_LINE_SIZE - 1
	JE	@B
; Perform backspacing
	PUSH	CS
	PUSH	str_backspace
	CALL	_serial_send_str

	INC	CX
	DEC	DI
	MOV	BYTE PTR SS:[DI], 0
	JMP	@B

__linerecv_normal::
; Apparently serial terminals use CR for end and LF is basically ignored
	CMP	AL, _LF
	JE	@B
	CMP	AL, _CR
	JE	@F

	PUSH	AX
	PUSH	AX
	CALL	_serial_send_byte
	POP	AX

	MOV	SS:[DI], AL
	INC	DI
	LOOP	@B
@@:
	MOV	BYTE PTR SS:[DI], 0
	CALL	_serial_send_newline
__linerecv_done::

	LEA	DI, SS:[SI].input_line
	MOV	AL, SS:[DI]
	XOR	AH, AH

	POP	BX
	POP	CX
	RET

__linerecv_cancel::
	MOV	BYTE PTR SS:[SI].input_line, 0
	PUSH	CS
	PUSH	str_lineclr
	CALL	_serial_send_str
	JMP	__linerecv_done

str_backspace::
	DB	01Bh,"[D ",01Bh,"[D",0

str_lineclr::
	DB	01Bh,"[2K",_CR,0

low_xlat::
;		 x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, xA, xB, xC, xD, xE, xF
	DB	  0,  0,  0,_CC,_CC,  0,  0,  0,_BS,  0,_LF,  0,  0,_CR,  0,  0 ; 0x
	DB	  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,_CC,  0,  0,  0,  0 ; 1x
high_xlat::
	DB	 96,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O' ; 6x
	DB	'P','Q','R','S','T','U','V','W','X','Y','Z','{','|','}','~',_BS ; 7x

_TEXT	ENDS

	END
