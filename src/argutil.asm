; Implements:
;   void argutil_strip(); // Skips whitespace
;   WORD argutil_next(); // Advances to next argument
;                        // 0 = Success, -1 = No next argument
;   BYTE argutil_reg(); // Gets register index
;                       // 0FFh = no register, 0FEh = bad register
;   WORD argutil_hex(); // Gets hex value
;   DWORD argutil_hex8(); // Gets 32bit hex value (DX:AX)

	.186

	INCLUDE	<top.inc>

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
; SS:SI is the work area!
	ASSUME	SI:PTR MonitorWork

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_strip
@@:
	INC	DI
_argutil_strip::
	MOV	AL, SS:[DI]
	CMP	AL, ' '
	JE	@B
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_find
; BL = find char
_argutil_find::
	MOV	AL, SS:[DI]
	CMP	AL, 0
	JE	@F
	INC	DI
	CMP	AL, BL
	JNE	_argutil_find
	MOV	AX, 0
	RET
@@:
	NOT	AX
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_next
_argutil_next::
	PUSH	BX
	MOV	BL, ','
	CALL	_argutil_find
	POP	BX
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_hex8
_argutil_hex8::
	PUSH	CX
	PUSH	BX
	PUSH	DI
	CALL	_argutil_strip
	XOR	DX, DX
	XOR	AX, AX
	XOR	BX, BX
@@:
	MOV	CX, AX
	ROL	CX, 4
	AND	CX, 0Fh
	SHL	DX, 4
	SHL	AX, 4
	OR	DX, CX
	OR	AX, BX
	XOR	BX, BX

	MOV	BL, SS:[DI]
	INC	DI
	CMP	BL, 'F'
	JG	@F
	SUB	BL, '0'
	JL	@F
	CMP	BL, 9
	JLE	@B
	SUB	BL, 'A'-'0'
	JL	@F
	ADD	BL, 10
	JMP	@B
@@:
	POP	DI
	POP	BX
	POP	CX
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_hex
_argutil_hex::
	PUSH	DX
	CALL	_argutil_hex8
	POP	DX
	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_argutil_reg
_argutil_reg::
	PUSH	CX

	CALL	_argutil_strip

	PUSH	ES
	MOV	AX, CS
	MOV	ES, AX

	MOV	AX, SS:[DI]
	MOV	CL, 0FFh
	CMP	AL, 0
	JE	@F
	MOV	CL, 13
	CMP	AL, 'F'
	JE	@F
	PUSH	DI
	MOV	DI, strtbl_reg_reverse
	MOV	CL, 13
	REPNZ SCASW
	POP	DI
	JE	@F
	MOV	CX, 0FEh
@@:
	MOV	AL, CL
	XOR	AH, AH

	POP	ES
	POP	CX
	RET

strtbl_reg_reverse:
	DB	"PC","SS","ES","DS","CS"
	DB	"DI","SI","BP","SP","BX","DX","CX","AX"

strtbl_reg:
	DB	"AX","CX","DX","BX","SP","BP","SI","DI"
	DB	"CS","DS","ES","SS","PC"

_TEXT	ENDS

	END
