; Only register that actually needs to be saved is SS:SI.

	.186

	INCLUDE	<top.inc>

	EXTRN	_serial_send_str:proc
	EXTRN	_monitor_main:proc
	EXTRN	_monitor_leave:proc
	EXTRN	_cmd_leave:proc
	EXTRN	_argutil_next:proc
	EXTRN	_argutil_hex:proc
	EXTRN	_argutil_hex8:proc
	EXTRN	_argutil_reg:proc
	EXTRN	_serial_recv_line:proc
	EXTRN	_serial_send_byte:proc
	EXTRN	_serial_send_hex:proc
	EXTRN	_serial_send_binary:proc
	EXTRN	_serial_send_newline:proc

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
	ASSUME	SI:PTR MonitorWork

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_unknown::
	PUSH	CS
	PUSH	str_unknown_left
	CALL	_serial_send_str

	PUSH	SS
	LEA	BX, SS:[SI].input_line
	PUSH	BX
	CALL	_serial_send_str

	PUSH	CS
	PUSH	str_unknown_right
	CALL	_serial_send_str

	JMP	_cmd_leave

str_unknown_left::
	DB	"Unknown command <",0

str_unknown_right::
	DB	">, ignoring.",_CR,_LF,0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_badargs::
	PUSH	CS
	PUSH	str_badargs
	CALL	_serial_send_str

	JMP	_cmd_leave

str_badargs::
	DB	"Invalid arguments, ignoring.",_CR,_LF,0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_reset::
	JMP	_monitor_main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_reg::
	PUSH	BX
	CBW
	ADD	AX, AX
	PUSH	AX
		ADD	AX, AX
		MOV	BX, AX
		ADD	BX, str_regprint
		PUSH	CS
		PUSH	BX
		CALL	_serial_send_str
	POP	AX
	MOV	BX, AX
	MOV	AX, SS:[SI+BX].regsave_ax
	CMP	BX, 26
	JE	@F
	PUSH	4
	PUSH	AX
	CALL	_serial_send_hex
	POP	BX
	RET
@@:
	PUSH	16
	PUSH	AX
	CALL	_serial_send_binary
	POP	BX
	RET

str_regprint::
	DB	"AX=",0,"CX=",0,"DX=",0,"BX=",0
	DB	"SP=",0,"BP=",0,"SI=",0,"DI=",0
	DB	"CS=",0,"DS=",0,"ES=",0,"SS=",0
	DB	"PC=",0,"F= ",0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_reg::
	CALL	_argutil_reg
	CMP	AL, 0FEh
	JAE	@F
; Register specified
	MOV	SS:[SI].cmd_work+0, AL
	CALL	print_reg
	CALL	_serial_send_newline
	CALL	_argutil_next
	TEST	AX, AX
	JNZ	_cmd_leave
	MOV	AL, SS:[DI]
	TEST	AL, AL
	JNZ	__reg_arg
	PUSH	'='
	CALL	_serial_send_byte
	CALL	_serial_recv_line
	CMP	AL, 0
	JE	_cmd_leave
__reg_arg::
; Set value for register
	CALL	_argutil_hex
	MOV	DX, AX
	MOV	AL, SS:[SI].cmd_work+0
	CBW
	ADD	AX, AX
	MOV	BX, AX
	MOV	SS:[SI+BX].regsave_ax, DX
	MOV	AL, SS:[SI].cmd_work+0
	CALL	print_reg
	CALL	_serial_send_newline
	JMP	_cmd_leave
@@:
	CMP	AL, 0FEh
	JNE	@F
; error
	PUSH	CS
	PUSH	str_badreg
	CALL	_serial_send_str
	JMP	_cmd_leave
@@:
	MOV	CX, 8
@@:
	MOV	AX, 8
	SUB	AX, CX
	CALL	print_reg
	PUSH	' '
	CALL	_serial_send_byte
	LOOP	@B

	CALL	_serial_send_newline

	MOV	CX, 6
@@:
	MOV	AX, 14
	SUB	AX, CX
	CALL	print_reg
	PUSH	' '
	CALL	_serial_send_byte
	LOOP	@B

	CALL	_serial_send_newline
	JMP	_cmd_leave

str_badreg::
	DB	"Unknown register",_CR,_LF,0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_go::
	JMP	_monitor_leave

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_portin::
	CALL	_argutil_hex
	MOV	DX, AX
	CALL	_argutil_next
	MOV	AL, SS:[DI]
	CMP	AL, 'B'
	JNZ	@F
	IN	AL, DX
	PUSH	2
	JMP	__portin_print
@@:
	IN	AX, DX
	PUSH	4
__portin_print::
	PUSH	AX
	CALL	_serial_send_hex
	CALL	_serial_send_newline
	JMP	_cmd_leave

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_portout::
	CALL	_argutil_hex
	MOV	DX, AX
	CALL	_argutil_next
	TEST	AX, AX
	JNZ	cmd_badargs
	CALL	_argutil_hex
	PUSH	AX
	CALL	_argutil_next
	MOV	BL, SS:[DI]
	POP	AX
	CMP	BL, 'B'
	JNZ	@F
	OUT	DX, AL
	JMP	_cmd_leave
@@:
	OUT	DX, AX
	JMP	_cmd_leave

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_dump::
	CALL	_argutil_hex8
	ROR	DX, 4
	AND	DX, 0F000h
	MOV	BX, AX
	AND	BX, 0Fh
	SHR	AX, 4
	OR	AX, DX
	MOV	ES, AX

	CALL	_argutil_next
	MOV	CX, 4
	TEST	AX, AX
	JNZ	__dumploop
	CALL	_argutil_hex8
	AND	DX, 0Fh
	ROR	DX, 4
	MOV	CX, AX
	ADD	CX, 0Fh
	SHR	CX, 4
	OR	CX, DX

__dumploop:
	PUSH	CX

	PUSH	4
	PUSH	ES
	CALL	_serial_send_hex
	PUSH	1
	PUSH	BX
	CALL	_serial_send_hex
	PUSH	':'
	CALL	_serial_send_byte
	PUSH	' '
	CALL	_serial_send_byte

	LEA	BP, SS:[SI].cmd_work
	MOV	CX, 010h
__dump_innerloop_bytes:
	MOV	AL, ES:[BX]
	MOV	SS:[BP], AL
	INC	BX
	INC	BP

	PUSH	2
	PUSH	AX
	CALL	_serial_send_hex
	CMP	CX, 09h
	JNE	__dump_notdash
	PUSH	'-'
	CALL	_serial_send_byte
	LOOP	__dump_innerloop_bytes
__dump_notdash:
	PUSH	' '
	CALL	_serial_send_byte
	LOOP	__dump_innerloop_bytes

	SUB	BX, 16
	MOV	AX, ES
	INC	AX
	MOV	ES, AX

	PUSH	' '
	CALL	_serial_send_byte

	LEA	BP, SS:[SI].cmd_work
	MOV	CX, 010h
__dump_innerloop_chars:
	MOV	AL, SS:[BP]
	INC	BP
	MOV	DL, '.'
	CMP	AL, 07Fh
	JGE	__dump_print
	CMP	AL, 020h
	JL	__dump_print
	MOV	DL, AL
__dump_print:
	PUSH	DX
	CALL	_serial_send_byte
	LOOP	__dump_innerloop_chars
	CALL	_serial_send_newline
	POP	CX
	LOOP	__dumploop

	JMP	_cmd_leave


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cmd_enter::
	CALL	_argutil_hex8
	ROR	DX, 4
	AND	DX, 0F000h
	MOV	BX, AX
	AND	BX, 0Fh
	SHR	AX, 4
	OR	AX, DX
	MOV	ES, AX

@@:
	CALL	_argutil_next
	TEST	AX, AX
	JNZ	@F
	CALL	_argutil_hex
	MOV	ES:[BX], AL
	INC	BX
	JMP	@B
@@:
	JMP	_cmd_leave

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_cmdtable
_cmdtable::
	DW	cmd_unknown	; Unknown
	DW	cmd_unknown	; ?
	DW	cmd_unknown	; A
	DW	cmd_unknown	; B
	DW	cmd_unknown	; C
	DW	cmd_dump	; D
	DW	cmd_enter	; E
	DW	cmd_unknown	; F
	DW	cmd_go		; G
	DW	cmd_unknown	; H
	DW	cmd_portin	; I
	DW	cmd_unknown	; J
	DW	cmd_unknown	; K
	DW	cmd_unknown	; L
	DW	cmd_unknown	; M
	DW	cmd_unknown	; N
	DW	cmd_portout	; O
	DW	cmd_unknown	; P
	DW	cmd_reset	; Q
	DW	cmd_reg		; R
	DW	cmd_unknown	; S
	DW	cmd_unknown	; T
	DW	cmd_unknown	; U
	DW	cmd_unknown	; V
	DW	cmd_unknown	; W
	DW	cmd_unknown	; X
	DW	cmd_unknown	; Y
	DW	cmd_unknown	; Z

_TEXT	ENDS

	END
