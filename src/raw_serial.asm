; Implements:
;   void serial_enable(void);
;   void serial_disable(void);
;   void serial_send_byte(BYTE data);
;   BYTE serial_recv_byte(void);

	.186

	INCLUDE	<top.inc>

REG_SER_DATA	equ	0B1h
REG_SER_STATUS	equ	0B3h

_TEXT	SEGMENT BYTE PUBLIC 'CODE'

; void serial_enable(void);
	PUBLIC	_serial_enable
_serial_enable::
	IN	AL, REG_SER_STATUS
	OR	AL, 0E0h
	OUT	REG_SER_STATUS, AL

; turn on ser rx irq
	IF	HAS_RX_IRQ
	 IN	AL, 0B2h
	 OR	AL, 08h
	 OUT	0B2h, AL
	ENDIF

	RET

; void serial_disable(void);
	PUBLIC	_serial_disable
_serial_disable::
@@:
	IN	AL, REG_SER_STATUS
	TEST	AL, 4
	JZ	@B

	AND	AL, 05Fh
	OUT	REG_SER_STATUS, AL
	RET

; void serial_send_byte(BYTE v);
	PUBLIC	_serial_send_byte
_serial_send_byte::
	PUSH	BP
	MOV	BP, SP
@@:
	IN	AL, REG_SER_STATUS
	TEST	AL, 4
	JZ	@B

	MOV	AL, [BP+4]
	OUT	REG_SER_DATA, AL

	POP	BP
	RET	2

; BYTE serial_recv_byte(void);
	PUBLIC	_serial_recv_byte
_serial_recv_byte::
	PUSH	BP
	MOV	BP, SP
	JMP	__loop_recv_byte_nohlt
__recv_overrun:
	OR	AL, 020h
	OUT	REG_SER_STATUS, AL
	JMP	__loop_recv_byte_nohlt
@@:
	IF	HAS_RX_IRQ
	 HLT
	ENDIF
__loop_recv_byte_nohlt:
	IN	AL, REG_SER_STATUS
	TEST	AL, 2
	JNZ	__recv_overrun
	TEST	AL, 1
	JZ	@B

	IN	AL, REG_SER_DATA

	POP	BP
	RET

_TEXT	ENDS

	END
