; Code info:
; ABI:
;   AX: Caller save. Return value.
;   CX: Callee save.
;   DX: Callee save.
;   BX: Callee save.
;   SI: Callee save. MonitorWork pointer (with SS:)
;   DI: Callee save. input_line pointer (with SS:)
;
;   SP: Callee save.
;   BP: Callee save.
;   DS: Callee save. Equals CS.
;   ES: Callee save. Equals SS.
;   SS: Callee save. V_WORK_SEG
;   Arguments passed on stack right to left.
;   Callee cleanup.
;
;   Public labels begin with underscore.
;   Private labels have no prefix.
;   Local labels should have two underscores.

	INCLUDE	<config.inc>

	INCLUDE	<memory.inc>
	INCLUDE	<defines.inc>
