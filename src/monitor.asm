	.186

	INCLUDE	<top.inc>

	EXTRN	_serial_enable:proc
	EXTRN	_serial_recv_byte:proc
	EXTRN	_serial_recv_line:proc
	EXTRN	_serial_send_byte:proc
	EXTRN	_serial_send_str:proc
	EXTRN	_serial_send_newline:proc
	EXTRN	_cmdtable:abs

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
	ASSUME	SI:PTR MonitorWork

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_monitor_enter_ncall
; SS should be equal to V_WORK_SEG
_monitor_enter_ncall::
	MOV	SS:[V_WORK_AREA+12], SI
	MOV	SI, V_WORK_AREA
	MOV	SS:[SI].regsave_ax, AX
	MOV	SS:[SI].regsave_bp, BP
	MOV	BP, SP
	MOV	SP, V_STACK_TOP
	PUSHF
	POP	AX
	MOV	SS:[SI].regsave_flag, AX
	MOV	AX, SS
	MOV	SS:[SI].regsave_ss, AX

	MOV	AX, [BP+0]
	MOV	SS:[SI].regsave_pc, AX
	MOV	AX, CS
	MOV	SS:[SI].regsave_cs, AX
	ADD	BP, 2
	JMP	monitor_enter_common

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_monitor_enter_fcall
; SS should be equal to V_WORK_SEG
_monitor_enter_fcall::
	MOV	SS:[V_WORK_AREA+12], SI
	MOV	SI, V_WORK_AREA
	MOV	SS:[SI].regsave_ax, AX
	MOV	SS:[SI].regsave_bp, BP
	MOV	BP, SP
	MOV	SP, V_STACK_TOP
	PUSHF
	POP	AX
	MOV	SS:[SI].regsave_flag, AX
	MOV	AX, SS
	MOV	SS:[SI].regsave_ss, AX

	MOV	AX, [BP+0]
	MOV	SS:[SI].regsave_pc, AX
	MOV	AX, [BP+2]
	MOV	SS:[SI].regsave_cs, AX
	ADD	BP, 4
	JMP	monitor_enter_common

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Requirements:
;   AX,SI,CS,SS,PC,BP,FLAG is already saved.
;   BP=SP _before_ entry!
;   SS:SI has been set to V_WORK_SEG:V_WORK_AREA
;   SP has been set to V_STACK_TOP
;   Everything else should be as it was.
monitor_enter_common:
	MOV	SS:[SI].regsave_sp, BP
	MOV	SS:[SI].regsave_cx, CX
	MOV	SS:[SI].regsave_dx, DX
	MOV	SS:[SI].regsave_bx, BX
	MOV	SS:[SI].regsave_di, DI
	MOV	AX, DS
	MOV	SS:[SI].regsave_ds, AX
	MOV	AX, ES
	MOV	SS:[SI].regsave_es, AX
	JMP	_monitor_main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_monitor_leave
_monitor_leave::
	MOV	AX, SS:[SI].regsave_ds
	MOV	DS, AX
	MOV	AX, SS:[SI].regsave_es
	MOV	ES, AX
	MOV	CX, SS:[SI].regsave_cx
	MOV	DX, SS:[SI].regsave_dx
	MOV	BX, SS:[SI].regsave_bx
	MOV	SP, SS:[SI].regsave_sp
	MOV	BP, SS:[SI].regsave_bp
	MOV	DI, SS:[SI].regsave_di
; Setup IRET stack
	MOV	AX, SS:[SI].regsave_flag
	PUSH	AX
	MOV	AX, SS:[SI].regsave_cs
	PUSH	AX
	MOV	AX, SS:[SI].regsave_pc
	PUSH	AX
; NOTE: As mentioned, SS needs to be V_WORK_SEG always.
; So this could blow up in a bad case.
	MOV	AX, SS:[SI].regsave_ss
	MOV	SS, AX
	MOV	AX, SS:[SI].regsave_ax
	MOV	SI, SS:[SI].regsave_si
	IRET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_monitor_main
_monitor_main::
; DS is CS
	MOV	AX, CS
	MOV	DS, AX
; ES is SS
	MOV	AX, SS
	MOV	ES, AX

	CALL	_serial_enable

	PUSH	CS
	PUSH	str_hello
	CALL	_serial_send_str

	PUBLIC	_cmd_leave
_cmd_leave::
mainloop::
	PUSH	CS
	PUSH	str_prompt
	CALL	_serial_send_str

	CALL	_serial_recv_line
	TEST	AX, AX
	JZ	_cmd_leave
	INC	DI

	MOV	BX, 1
	CMP	AL, '?'
	JE	cmd_exec
	MOV	BX, 0
	CMP	AL, 'Z'
	JG	cmd_exec
	SUB	AL, 'A'
	JL	cmd_exec
	ADD	AL, 2
	MOV	BX, AX
cmd_exec::
	ADD	BX, BX
	JMP	WORD PTR [_cmdtable+BX]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
str_hello::
	DB	01Bh,"[2J",01Bh,"[H"
	DB	"wsmonitor v1 (C)2016 Alex 'trap15' Marshall",_CR,_LF,0

str_prompt::
	DB	_CR,_LF,"mon> ",0

_TEXT	ENDS

	END
