	.186

	INCLUDE	<top.inc>

	EXTRN	_monitor_enter_ncall:proc

_TEXT		SEGMENT BYTE PUBLIC 'CODE'
_TEXT		ENDS

CGROUP	GROUP	_TEXT

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
_start::
	CLI
	CLD
	MOV	AX, V_WORK_SEG
	MOV	SS, AX
	JMP	_monitor_enter_ncall

_TEXT	ENDS

; set entrypoint to _start
	END	_start
